\newpage

* Table of Contents                                 :TOC_5_org:noexport:
:PROPERTIES:
:VISIBILITY: all
:END:

- [[Overview][Overview]]
  - [[Scope][Scope]]
  - [[Purpose][Purpose]]
  - [[Structure][Structure]]
- [[Links][Links]]
- [[How to …][How to …]]
  - [[… produce pdf][… produce pdf]]
  - [[… troubleshooting][… troubleshooting]]
  - [[… tangle code][… tangle code]]
- [[Tables][Tables]]
  - [[A table, centered][A table, centered]]
    - [[Almost same table, left aligned][Almost same table, left aligned]]
- [[EBNF][EBNF]]
  - [[Verbatim][Verbatim]]
  - [[Example][Example]]
  - [[Src vhdl][Src vhdl]]
  - [[Verse][Verse]]
- [[Images][Images]]
- [[A couple of rtls][A couple of rtls]]
  - [[Trigger][Trigger]]
    - [[Entity][Entity]]
    - [[Architecture][Architecture]]
    - [[Architecture body][Architecture body]]
      - [[Reset][Reset]]
      - [[Process][Process]]
  - [[Counter][Counter]]
    - [[Libraries][Libraries]]
      - [[Packages][Packages]]
    - [[Entity][Entity]]
    - [[Architecture][Architecture]]
      - [[Types][Types]]
      - [[Signals][Signals]]
      - [[Architecture body][Architecture body]]
        - [[*Process*][*Process*]]
        - [[*Instance*][*Instance*]]
- [[Left to do][Left to do]]
- [[Footnotes][Footnotes]]

* Overview

Aliquam erat volutpat. Nunc eleifend leo vitae magna. In id erat non orci
commodo lobortis. Proin neque massa, cursus ut, gravida ut, lobortis eget,
lacus. Sed diam. Praesent fermentum tempor tellus. Nullam tempus. Mauris ac
felis vel velit tristique imperdiet. Donec at pede. Etiam vel neque nec dui
dignissim bibendum. Vivamus id enim. Phasellus neque orci, porta a, aliquet
quis, semper a, massa. Phasellus purus. Pellentesque tristique imperdiet tortor.
Nam euismod tellus id erat.

** Scope

Pellentesque dapibus suscipit ligula. Donec posuere augue in quam. Etiam vel
tortor sodales tellus ultricies commodo. Suspendisse potenti. Aenean in sem ac
leo mollis blandit. Donec neque quam, dignissim in, mollis nec, sagittis eu,
wisi. Phasellus lacus. Etiam laoreet quam sed arcu. Phasellus at dui in ligula
mollis ultricies. Integer placerat tristique nisl. Praesent augue. Fusce
commodo. Vestibulum convallis, lorem a tempus semper, dui dui euismod elit,
vitae placerat urna tortor vitae lacus. Nullam libero mauris, consequat quis,
varius et, dictum id, arcu. Mauris mollis tincidunt felis. Aliquam feugiat
tellus ut neque. Nulla facilisis, risus a rhoncus fermentum, tellus tellus
lacinia purus, et dictum nunc justo sit amet elit.

** Purpose

Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec hendrerit tempor
tellus. Donec pretium posuere tellus. Proin quam nisl, tincidunt et, mattis
eget, convallis nec, purus. Cum sociis natoque penatibus et magnis dis
parturient montes, nascetur ridiculus mus. Nulla posuere. Donec vitae dolor.
Nullam tristique diam non turpis. Cras placerat accumsan nulla. Nullam rutrum.
Nam vestibulum accumsan nisl.

** Structure

Nullam eu ante vel est convallis dignissim. Fusce suscipit, wisi nec facilisis
facilisis, est dui fermentum leo, quis tempor ligula erat quis odio. Nunc porta
vulputate tellus. Nunc rutrum turpis sed pede. Sed bibendum. Aliquam posuere.
Nunc aliquet, augue nec adipiscing interdum, lacus tellus malesuada massa, quis
varius mi purus non odio. Pellentesque condimentum, magna ut suscipit hendrerit,
ipsum augue ornare nulla, non luctus diam neque sit amet urna. Curabitur
vulputate vestibulum lorem. Fusce sagittis, libero non molestie mollis, magna
orci ultrices dolor, at vulputate neque nulla lacinia eros. Sed id ligula quis
est convallis tempor. Curabitur lacinia pulvinar nibh. Nam a sapien.

\newpage

* Links

Here we go:

 - [[*A couple of rtls][A couple of rtls]]
 - [[*Tables][Tables]]
 - [[*Footnotes][Footnotes]]
 - [[*Images][Images]]

#+CAPTION: This is the caption for the next figure link (or table)
#+NAME:   fig:SED-HR4049
[[file:pics/image.png]]

\newpage

* How to …

/Note that text in comment blocs doesn’t get exported./

#+begin_comment
  Aliquam erat volutpat. Nunc eleifend leo vitae magna. In id erat non orci
  commodo lobortis. Proin neque massa, cursus ut, gravida ut, lobortis eget,
  lacus. Sed diam. Praesent fermentum tempor tellus. Nullam tempus. Mauris ac
  felis vel velit tristique imperdiet. Donec at pede. Etiam vel neque nec dui
  dignissim bibendum. Vivamus id enim. Phasellus neque orci, porta a, aliquet
  quis, semper a, massa. Phasellus purus. Pellentesque tristique imperdiet tortor.
  Nam euismod tellus id erat.
#+end_comment

** … produce pdf

You’ll may produce a pdf version of this document by running emacs in batch
mode [fn:1]

#+begin_src sh
  emacs -q lrm.org --batch --eval="(org-latex-export-to-pdf)"
#+end_src

In order to produce a source colour version of the exported rtl code, you’ll
have to use ~Minted~, and you’ll need to install the companion program =pygments=.
You can do this with

#+begin_src sh
  pip install pygments
#+end_src

And finally run

#+begin_src sh
  emacs -q -l ./lrm.el lrm.org --batch --eval="(org-latex-export-to-pdf)"
#+end_src

\noindent where emacs’ =lrm.el= init file includes all necessary setup.

** … troubleshooting

If emacs complies with a message like

#+begin_quote
  “no org-babel-execute function for sh”
#+end_quote

\noindent meaning that no library for exporting is loaded. Fix it evaluting the following
(put the cursor at the end of last parenthesis and do a =C-x C-e=) or select the
three lines an do a =Mx eval-region=.

#+begin_src emacs-lisp
  (org-babel-do-load-languages
   'org-babel-load-languages
   '((shell . t)))
#+end_src

** … tangle code

/You’ll be able to extract vhdl code with the following [fn:2]/

#+begin_src sh
  emacs -q -l ./lrm-tangle.el lrm.org --batch --eval="(org-babel-tangle)"
#+end_src

\noindent where emacs’ =lrm-tangle.el= init file includes a hook to beautufy extracted code.

\newpage

* Tables

** A table, centered

Pellentesque dapibus suscipit ligula. Donec posuere augue in quam. Etiam vel
tortor sodales tellus ultricies commodo. Suspendisse potenti. Aenean in sem ac
leo mollis blandit. Donec neque quam, dignissim in, mollis nec, sagittis eu,
wisi. Phasellus lacus. Etiam laoreet quam sed arcu. Phasellus at dui in ligula
mollis ultricies. Integer placerat tristique nisl. Praesent augue. Fusce
commodo. Vestibulum convallis, lorem a tempus semper, dui dui euismod elit,
vitae placerat urna tortor vitae lacus. Nullam libero mauris, consequat quis,
varius et, dictum id, arcu. Mauris mollis tincidunt felis. Aliquam feugiat
tellus ut neque. Nulla facilisis, risus a rhoncus fermentum, tellus tellus
lacinia purus, et dictum nunc justo sit amet elit.

#+CAPTION: This is a caption for this table
#+NAME:   tab:table1
#+ATTR_LaTeX: :align c c c
|      Équipe      |        Référence         | Characteristiques |
|                  |        Fabricant         |    Techniques     |
|                  |                          |                   |
|                  |                          |                   |
| Géné de Fonction |    Gw Instek SFG-2120    |                   |
| Géné de Fonction |      Agilent 33220A      |      20 MHz       |
|                  |                          |                   |
|   Oscilloscope   |   Tektronix TDS8206GHz   |       6 GHz       |
|   Oscilloscope   | Tektronix TDS3034B300MHz |      300 MHz      |
|   Oscilloscope   |   Tektronix 222550MHz    |      50 MHz       |
|   Oscilloscope   |    Lecroy 9420350MHz     |      350 MHz      |
|   Oscilloscope   |   Lecroy Wave Ace 234    |                   |
|   Oscilloscope   |     Tektro TDS3034C      |      300MHz       |

*** Almost same table, left aligned

Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec hendrerit tempor
tellus. Donec pretium posuere tellus. Proin quam nisl, tincidunt et, mattis
eget, convallis nec, purus. Cum sociis natoque penatibus et magnis dis
parturient montes, nascetur ridiculus mus. Nulla posuere. Donec vitae dolor.
Nullam tristique diam non turpis. Cras placerat accumsan nulla. Nullam rutrum.
Nam vestibulum accumsan nisl.

Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec hendrerit tempor
tellus. Donec pretium posuere tellus. Proin quam nisl, tincidunt et, mattis
eget, convallis nec, purus. Cum sociis natoque penatibus et magnis dis
parturient montes, nascetur ridiculus mus. Nulla posuere. Donec vitae dolor.
Nullam tristique diam non turpis. Cras placerat accumsan nulla. Nullam rutrum.
Nam vestibulum accumsan nisl.

#+CAPTION: This is a caption for this table
#+NAME:   tab:table2
#+ATTR_LaTeX: :align l l l
| Équipe           | Référence                | Characteristiques                            |
|                  | Fabricant                | Techniques                                   |
|                  |                          |                                              |
|                  |                          |                                              |
| Géné de Fonction | Gw Instek SFG-2120       |                                              |
| Géné de Fonction | Agilent 33220A           | 20 MHz                                       |
|                  |                          |                                              |
| Oscilloscope     | Tektronix TDS8206GHz     | 6 GHz                                        |
| Oscilloscope     | Tektronix TDS3034B300MHz | 300 MHz                                      |
| Oscilloscope     | Tektronix 222550MHz      | 50 MHz                                       |
| Oscilloscope     | Lecroy 9420350MHz        | 350 MHz                                      |
| Oscilloscope     | Lecroy Wave Ace 234      |                                              |
| Oscilloscope     | Tektro TDS3034C          | Donec hendrerit tempor tellus.               |
|                  |                          | Nullam rutrum.  Donec posuere augue in quam. |
|                  |                          | Pellentesque tristique imperdiet tortor.     |
|                  |                          | Nunc aliquet, augue nec adipiscing interdum, |
|                  |                          | lacus tellus malesuada massa, quis varius mi |
|                  |                          | purus non odio.                              |

Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec hendrerit tempor
tellus. Donec pretium posuere tellus. Proin quam nisl, tincidunt et, mattis
eget, convallis nec, purus. Cum sociis natoque penatibus et magnis dis
parturient montes, nascetur ridiculus mus. Nulla posuere. Donec vitae dolor.
Nullam tristique diam non turpis. Cras placerat accumsan nulla. Nullam rutrum.
Nam vestibulum accumsan nisl.

\newpage

* EBNF

Let’s try several different contexts to see the rendering

\newpage

** Verbatim

#+begin_verbatim
  component_configuration ::=
     *for* component_specification
        [ binding_indication ; ]
        { verification_unit_binding_indication ; }
        [ block_configuration ]
     *end for* ;
#+end_verbatim

#+begin_verbatim
   *subprogram_declaration* ::=
      subprogram_specification ;

   /subprogram_specification/ ::=
      procedure_specification | function_specification

   _procedure_specification_ ::=
      procedure designator
         subprogram_header
         [ [ parameter ] ( formal_parameter_list ) ]

   ~function_specification~ ::=
      [ pure | impure ] function designator
      subprogram_header
      [ [ parameter ] ( formal_parameter_list ) ] return type_mark

   =subprogram_header= ::=
      [ generic ( generic_list )
      [ generic_map_aspect ] ]

   designator ::= identifier | operator_symbol

   operator_symbol ::= string_literal
#+end_verbatim

\newpage

** Example

#+begin_example
  component_configuration ::=
     for component_specification
        [ binding_indication ; ]
        { verification_unit_binding_indication ; }
        [ block_configuration ]
     end for ;
#+end_example

#+begin_example
   *subprogram_declaration* ::=
      subprogram_specification ;

   /subprogram_specification/ ::=
      procedure_specification | function_specification

   _procedure_specification_ ::=
      procedure designator
         subprogram_header
         [ [ parameter ] ( formal_parameter_list ) ]

   ~function_specification~ ::=
      [ pure | impure ] function designator
      subprogram_header
      [ [ parameter ] ( formal_parameter_list ) ] return type_mark

   =subprogram_header= ::=
      [ generic ( generic_list )
      [ generic_map_aspect ] ]

   designator ::= identifier | operator_symbol

   operator_symbol ::= string_literal
#+end_example

\newpage

** Src vhdl

#+begin_src vhdl
  component_configuration ::=
     for component_specification
        [ binding_indication ; ]
        { verification_unit_binding_indication ; }
        [ block_configuration ]
     end for ;
#+end_src

#+begin_src vhdl
   *subprogram_declaration* ::=
      subprogram_specification ;

   /subprogram_specification/ ::=
      procedure_specification | function_specification

   _procedure_specification_ ::=
      procedure designator
         subprogram_header
         [ [ parameter ] ( formal_parameter_list ) ]

   function_specification ::=
      [ pure | impure ] function designator
      subprogram_header
      [ [ parameter ] ( formal_parameter_list ) ] return type_mark

   =subprogram_header= ::=
      [ generic ( generic_list )
      [ generic_map_aspect ] ]

   designator ::= identifier | operator_symbol

   operator_symbol ::= string_literal
#+end_src

\newpage

** Verse

#+begin_verse
  component_configuration ::=
     *for* component_specification
        [ binding_indication ; ]
        { verification_unit_binding_indication ; }
        [ block_configuration ]
     *end for* ;
#+end_verse

#+begin_verse
   *subprogram_declaration* ::=
      subprogram_specification ;

   /subprogram_specification/ ::=
      procedure_specification | function_specification

   _procedure_specification_ ::=
      procedure designator
         subprogram_header
         [ [ parameter ] ( formal_parameter_list ) ]

   ~function_specification~ ::=
      [ pure | impure ] function designator
      subprogram_header
      [ [ parameter ] ( formal_parameter_list ) ] return type_mark

   =subprogram_header= ::=
      [ generic ( generic_list )
      [ generic_map_aspect ] ]

   designator ::= identifier | operator_symbol

   operator_symbol ::= string_literal
#+end_verse

\newpage

* Images

Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec hendrerit tempor
tellus. Donec pretium posuere tellus. Proin quam nisl, tincidunt et, mattis
eget, convallis nec, purus. Cum sociis natoque penatibus et magnis dis
parturient montes, nascetur ridiculus mus. Nulla posuere. Donec vitae dolor.
Nullam tristique diam non turpis. Cras placerat accumsan nulla. Nullam rutrum.
Nam vestibulum accumsan nisl.

#+CAPTION: This is the caption for the next figure link (or table)
#+NAME:   fig:SED-HR4049
[[file:pics/image.png]]

Aliquam erat volutpat. Nunc eleifend leo vitae magna. In id erat non orci
commodo lobortis. Proin neque massa, cursus ut, gravida ut, lobortis eget,
lacus. Sed diam. Praesent fermentum tempor tellus. Nullam tempus. Mauris ac
felis vel velit tristique imperdiet. Donec at pede. Etiam vel neque nec dui
dignissim bibendum. Vivamus id enim. Phasellus neque orci, porta a, aliquet
quis, semper a, massa. Phasellus purus. Pellentesque tristique imperdiet tortor.
Nam euismod tellus id erat.

\newpage

* A couple of rtls

#+begin_center
 /See [[*… tangle code][… tangle code]] on how to extract code./ \\
 Note how the :tangle property sets the place where the code is extracted.
#+end_center

Nullam eu ante vel est convallis dignissim. Fusce suscipit, wisi nec facilisis
facilisis, est dui fermentum leo, quis tempor ligula erat quis odio. Nunc porta
vulputate tellus. Nunc rutrum turpis sed pede. Sed bibendum. Aliquam posuere.
Nunc aliquet, augue nec adipiscing interdum, lacus tellus malesuada massa, quis
varius mi purus non odio. Pellentesque condimentum, magna ut suscipit hendrerit,
ipsum augue ornare nulla, non luctus diam neque sit amet urna. Curabitur[fn:3]
vulputate vestibulum lorem. Fusce sagittis, libero non molestie mollis, magna
orci ultrices dolor, at vulputate neque nulla lacinia eros. Sed id ligula quis
est convallis tempor. Curabitur lacinia pulvinar nibh. Nam a sapien.

** Trigger
:PROPERTIES:
:header-args:    :tangle rtl/trigger.vhd :mkdirp yes :tangle-mode (identity #o444)
:END:

Aliquam erat volutpat. Nunc eleifend leo vitae magna. In id erat non orci
commodo lobortis. Proin neque massa, cursus ut, gravida ut, lobortis eget,
lacus. Sed diam. Praesent fermentum tempor tellus. Nullam tempus. Mauris ac
felis vel velit tristique imperdiet. Donec at pede. Etiam vel neque nec dui
dignissim bibendum. Vivamus id enim. Phasellus neque orci, porta a, aliquet
quis, semper a, massa. Phasellus purus. Pellentesque tristique imperdiet tortor.
Nam euismod tellus id erat.

*** Entity

#+begin_src vhdl :tangle rtl/trigger_entity.vhd
  library IEEE;

  use IEEE.STD_LOGIC_1164.all;
  use IEEE.numeric_std_unsigned.all;
  use work.common_package.all;

  entity external_trigger is
    port (global_reset    : in    std_logic;
          p_if            : inout rc_peripheral_if_p2;
          StartSys        : in    std_logic;
          ExternalTrigger : out   std_logic;
          io_fpga1        : in    std_logic);
  end entity external_trigger;
#+end_src

*** Architecture

#+begin_src vhdl :tangle rtl/trigger_architecture.vhd
  architecture Behavioral of external_trigger is

    signal counter_trig_ext    : std_logic_vector(15 downto 0) := (others => '0');
    -- Temp trig ext to be oored with external signal
    signal trig_ext_inner, rst : std_logic                     := '0';

    alias EnableExternalTrigger : std_logic is p_if.params(0)(9);
    alias enable                : std_logic is p_if.params(0)(8);

  begin
#+end_src

*** Architecture body

**** Reset

#+begin_src vhdl
  rst <= global_reset or not(p_if.enable) or not(StartSys);
#+end_src

**** Process

#+begin_src vhdl
  U_trig_ext : process (p_if.clk) is
  begin
    if rising_edge(p_if.clk) then
      if rst = '1' then
        counter_trig_ext <= (others => '0');
        trig_ext_inner   <= '0';
      else
        if counter_trig_ext >= (p_if.params(0)(13 downto 10) & X"800") then
          trig_ext_inner   <= enable;
          counter_trig_ext <= (others => '0');
        else
          trig_ext_inner   <= '0';
          counter_trig_ext <= counter_trig_ext + 1;
        end if;
      end if;
    end if;
  end process U_trig_ext;

  ExternalTrigger <= trig_ext_inner or (io_fpga1 and EnableExternalTrigger);
#+end_src

#+begin_src vhdl
  end architecture Behavioral;
#+end_src

** Counter
:PROPERTIES:
:header-args:    :tangle rtl/p1_counter.vhd :mkdirp yes :tangle-mode (identity #o444)
:END:

*** Libraries

Curabitur lacinia pulvinar nibh.

#+begin_src vhdl
  library IEEE;
#+end_src

**** Packages

Etiam laoreet quam sed arcu.

#+begin_src vhdl
  use ieee.std_logic_1164.all;
  use ieee.numeric_std_unsigned.all;
  use work.common_package.all;
#+end_src

*** Entity

Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec hendrerit tempor
tellus. Donec pretium posuere tellus. Proin quam nisl, tincidunt et, mattis
eget, convallis nec, purus. Cum sociis natoque penatibus et magnis dis
parturient montes, nascetur ridiculus mus. Nulla posuere. Donec vitae dolor.
Nullam tristique diam non turpis. Cras placerat accumsan nulla. Nullam rutrum.
Nam vestibulum accumsan nisl.

#+begin_src vhdl
  entity p1_counter is
    port (clk          : in    std_logic;
          global_reset : in    std_logic;
          p_if         : inout rc_peripheral_if_p1);
  end entity p1_counter;
#+end_src

*** Architecture

Nullam eu ante vel est convallis dignissim. Fusce suscipit, wisi nec facilisis
facilisis, est dui fermentum leo, quis tempor ligula erat quis odio. Nunc porta
vulputate tellus. Nunc rutrum turpis sed pede. Sed bibendum. Aliquam posuere.
Nunc aliquet, augue nec adipiscing interdum, lacus tellus malesuada massa, quis
varius mi purus non odio. Pellentesque condimentum, magna ut suscipit hendrerit,
ipsum augue ornare nulla, non luctus diam neque sit amet urna. Curabitur
vulputate vestibulum lorem. Fusce sagittis, libero non molestie mollis, magna
orci ultrices dolor, at vulputate neque nulla lacinia eros. Sed id ligula quis
est convallis tempor. Curabitur lacinia pulvinar nibh. Nam a sapien.

#+begin_src vhdl
  architecture Behavioral of p1_counter is
#+end_src

**** Types

Donec neque quam, dignissim in, mollis nec, sagittis eu, wisi [fn:4].

#+begin_src vhdl
  type command1_state_type is (command1_state_1, command1_state_2);
#+end_src

**** Signals

#+begin_src vhdl
  signal fifo1_clear        : std_logic                     := '0';
  signal fifo1_full         : std_logic                     := '0';
  signal fifo1_wrreq        : std_logic                     := '0';
  signal fifo1_dataIn       : std_logic_vector(31 downto 0) := (others => '0');
  signal fifo1_dataIn1      : t_Byte                        := X"00";
  signal fifo1_dataIn2      : t_Byte                        := X"01";
  signal fifo1_dataIn3      : t_Byte                        := X"02";
  signal fifo1_dataIn4      : t_Byte                        := X"03";
  signal fill_fifo1_counter : std_logic_vector(31 downto 0) := (others => '0');
  signal command1_state     : command1_state_type;
#+end_src

**** Architecture body

Pellentesque dapibus suscipit ligula. Donec posuere augue in quam. Etiam vel
tortor sodales tellus ultricies commodo. Suspendisse potenti. Aenean in sem ac
leo mollis blandit. Donec neque quam, dignissim in, mollis nec, sagittis eu,
wisi. Phasellus lacus. Etiam laoreet quam sed arcu. Phasellus at dui in ligula
mollis ultricies. Integer placerat tristique nisl. Praesent augue. Fusce
commodo. Vestibulum convallis, lorem a tempus semper, dui dui euismod elit,
vitae placerat urna tortor vitae lacus. Nullam libero mauris, consequat quis,
varius et, dictum id, arcu. Mauris mollis tincidunt felis. Aliquam feugiat
tellus ut neque. Nulla facilisis, risus a rhoncus fermentum, tellus tellus
lacinia purus, et dictum nunc justo sit amet elit.

#+begin_src vhdl
  begin
#+end_src

#+begin_src vhdl
  fifo1_clear <= not(p_if.enable);
#+end_src

***** *Process*

Nullam eu ante vel est convallis dignissim. Fusce suscipit, wisi nec facilisis
facilisis, est dui fermentum leo, quis tempor ligula erat quis odio. Nunc porta
vulputate tellus. Nunc rutrum turpis sed pede. Sed bibendum. Aliquam posuere.
Nunc aliquet, augue nec adipiscing interdum, lacus tellus malesuada massa, quis
varius mi purus non odio. Pellentesque condimentum, magna ut suscipit hendrerit,
ipsum augue ornare nulla, non luctus diam neque sit amet urna. Curabitur
vulputate vestibulum lorem. Fusce sagittis, libero non molestie mollis, magna
orci ultrices dolor, at vulputate neque nulla lacinia eros. Sed id ligula quis
est convallis tempor. Curabitur lacinia pulvinar nibh. Nam a sapien.

#+begin_src vhdl
  u_p : process(clk)
  begin
    if rising_edge(clk) then
      if (global_reset = '1' or p_if.enable = '0') then
        fifo1_dataIn1  <= X"00";
        fifo1_dataIn2  <= X"01";
        command1_state <= command1_state_1;
      else
        case command1_state is
          when command1_state_1 =>
            fifo1_wrreq <= '0';
            if (fill_fifo1_counter < p_if.params(0)) and
              (fifo1_full = '0') then
              command1_state <= command1_state_2;
            end if;
          when command1_state_2 =>
            if (fifo1_full = '0') then
              fifo1_dataIn <= fifo1_dataIn1 &fifo1_dataIn2
                              &fifo1_dataIn3 &fifo1_dataIn4;
              fifo1_dataIn1      <= fifo1_dataIn1 + 4;
              fifo1_dataIn2      <= fifo1_dataIn2 + 4;
              fifo1_dataIn3      <= fifo1_dataIn3 + 4;
              fifo1_dataIn4      <= fifo1_dataIn4 + 4;
              fill_fifo1_counter <= fill_fifo1_counter + 4;
              fifo1_wrreq        <= '1';
              command1_state     <= command1_state_1;
            else
              fifo1_wrreq    <= '0';
              command1_state <= command1_state_2;
            end if;
        end case;
      end if;
    end if;
  end process;
#+end_src

***** *Instance*

Pellentesque dapibus suscipit ligula. Donec posuere augue in quam. Etiam vel
tortor sodales tellus ultricies commodo. Suspendisse potenti. Aenean in sem ac
leo mollis blandit. Donec neque quam, dignissim in, mollis nec, sagittis eu,
wisi. Phasellus lacus. Etiam laoreet quam sed arcu. Phasellus at dui in ligula
mollis ultricies. Integer placerat tristique nisl. Praesent augue. Fusce
commodo. Vestibulum convallis, lorem a tempus semper, dui dui euismod elit,
vitae placerat urna tortor vitae lacus. Nullam libero mauris, consequat quis,
varius et, dictum id, arcu. Mauris mollis tincidunt felis. Aliquam feugiat
tellus ut neque. Nulla facilisis, risus a rhoncus fermentum, tellus tellus
lacinia purus, et dictum nunc justo sit amet elit.

#+begin_src vhdl
  Cati_output_fifo1_inst : entity work.Cati_output_fifo1
    port map (rst    => fifo1_clear,
              wr_clk => clk,
              rd_clk => p_if.clk,
              din    => fifo1_dataIn,
              wr_en  => fifo1_wrreq,
              rd_en  => p_if.rd,
              dout   => p_if.data,
              full   => fifo1_full,
              empty  => p_if.empty);
#+end_src

#+begin_src vhdl
  end architecture Behavioral;
#+end_src

\newpage

* Left to do

What’s left

 - Fine tune depth of headings (H: parameter in setup file)

* Document Setup                                                   :noexport:

Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec hendrerit tempor
tellus. Donec pretium posuere tellus. Proin quam nisl, tincidunt et, mattis
eget, convallis nec, purus. Cum sociis natoque penatibus et magnis dis
parturient montes, nascetur ridiculus mus. Nulla posuere. Donec vitae dolor.
Nullam tristique diam non turpis. Cras placerat accumsan nulla. Nullam rutrum.
Nam vestibulum accumsan nisl.

#+setupfile: setup.org

\newpage

* Footnotes

[fn:4] Types are a weird beast

[fn:3] Footnotes are funny

[fn:1] Created under Emacs 28.0.50 (Org mode 9.3.6)

[fn:2] To execute commands from emacs, just move the cursor to the code bloc,
and do a =C-c C-c= (Control + c twice)
