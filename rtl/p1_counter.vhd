library IEEE;

use ieee.std_logic_1164.all;
use ieee.numeric_std_unsigned.all;
use work.common_package.all;

entity p1_counter is
  port (clk          : in    std_logic;
        global_reset : in    std_logic;
        p_if         : inout rc_peripheral_if_p1);
end entity p1_counter;

architecture Behavioral of p1_counter is

  type command1_state_type is (command1_state_1, command1_state_2);

  signal fifo1_clear        : std_logic                     := '0';
  signal fifo1_full         : std_logic                     := '0';
  signal fifo1_wrreq        : std_logic                     := '0';
  signal fifo1_dataIn       : std_logic_vector(31 downto 0) := (others => '0');
  signal fifo1_dataIn1      : t_Byte                        := X"00";
  signal fifo1_dataIn2      : t_Byte                        := X"01";
  signal fifo1_dataIn3      : t_Byte                        := X"02";
  signal fifo1_dataIn4      : t_Byte                        := X"03";
  signal fill_fifo1_counter : std_logic_vector(31 downto 0) := (others => '0');
  signal command1_state     : command1_state_type;

begin

  fifo1_clear <= not(p_if.enable);

  u_p : process(clk)
  begin
    if rising_edge(clk) then
      if (global_reset = '1' or p_if.enable = '0') then
        fifo1_dataIn1  <= X"00";
        fifo1_dataIn2  <= X"01";
        command1_state <= command1_state_1;
      else
        case command1_state is
          when command1_state_1 =>
            fifo1_wrreq <= '0';
            if (fill_fifo1_counter < p_if.params(0)) and
              (fifo1_full = '0') then
              command1_state <= command1_state_2;
            end if;
          when command1_state_2 =>
            if (fifo1_full = '0') then
              fifo1_dataIn <= fifo1_dataIn1 &fifo1_dataIn2
                              &fifo1_dataIn3 &fifo1_dataIn4;
              fifo1_dataIn1      <= fifo1_dataIn1 + 4;
              fifo1_dataIn2      <= fifo1_dataIn2 + 4;
              fifo1_dataIn3      <= fifo1_dataIn3 + 4;
              fifo1_dataIn4      <= fifo1_dataIn4 + 4;
              fill_fifo1_counter <= fill_fifo1_counter + 4;
              fifo1_wrreq        <= '1';
              command1_state     <= command1_state_1;
            else
              fifo1_wrreq    <= '0';
              command1_state <= command1_state_2;
            end if;
        end case;
      end if;
    end if;
  end process;

  Cati_output_fifo1_inst : entity work.Cati_output_fifo1
    port map (rst    => fifo1_clear,
              wr_clk => clk,
              rd_clk => p_if.clk,
              din    => fifo1_dataIn,
              wr_en  => fifo1_wrreq,
              rd_en  => p_if.rd,
              dout   => p_if.data,
              full   => fifo1_full,
              empty  => p_if.empty);

end architecture Behavioral;
