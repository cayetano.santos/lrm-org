library IEEE;

use IEEE.STD_LOGIC_1164.all;
use IEEE.numeric_std_unsigned.all;
use work.common_package.all;

entity external_trigger is
  port (global_reset    : in    std_logic;
        p_if            : inout rc_peripheral_if_p2;
        StartSys        : in    std_logic;
        ExternalTrigger : out   std_logic;
        io_fpga1        : in    std_logic);
end entity external_trigger;
