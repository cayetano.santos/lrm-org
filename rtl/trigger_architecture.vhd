architecture Behavioral of external_trigger is

  signal counter_trig_ext    : std_logic_vector(15 downto 0) := (others => '0');
  -- Temp trig ext to be oored with external signal
  signal trig_ext_inner, rst : std_logic                     := '0';

  alias EnableExternalTrigger : std_logic is p_if.params(0)(9);
  alias enable                : std_logic is p_if.params(0)(8);

begin
