rst <= global_reset or not(p_if.enable) or not(StartSys);

U_trig_ext : process (p_if.clk) is
begin
  if rising_edge(p_if.clk) then
    if rst = '1' then
      counter_trig_ext <= (others => '0');
      trig_ext_inner   <= '0';
    else
      if counter_trig_ext >= (p_if.params(0)(13 downto 10) & X"800") then
        trig_ext_inner   <= enable;
        counter_trig_ext <= (others => '0');
      else
        trig_ext_inner   <= '0';
        counter_trig_ext <= counter_trig_ext + 1;
      end if;
    end if;
  end if;
end process U_trig_ext;

ExternalTrigger <= trig_ext_inner or (io_fpga1 and EnableExternalTrigger);

end architecture Behavioral;
