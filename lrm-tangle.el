(add-hook 'org-babel-post-tangle-hook
          (lambda ()
            (when (string= major-mode "vhdl-mode")
              (vhdl-beautify-buffer))))
